(
    function () {
        // Заводим новый блок
        var navAddBlock = {};

        // Регистрация
        WindCore.blockRegister(
            // Новый блок объединяется с существующим "WindBlock"
            $.extend(
                navAddBlock,
                WindBlock, {
                    // Устанавливаем родительский блок
                    getSelector: function () {
                        return '.js-nav-add';
                    },
                    // Возвращает массив объектов с которыми работаем
                    getBindings: function () {
                        // Сохраняем текущий контекст
                        var self = this;
                        return [
                            // Клик на контроллер таба и смена таба
                            [
                                'click',
                                '.js-nav-add-tab-controller',
                                function (event) {
                                    event.preventDefault();
                                    $('.js-nav-add-tab-controller').removeClass('-active');
                                    $(this).addClass('-active');
                                    var numberTab = $(this).data('tab');

                                    var $tabContent = $('.js-nav-add-tab-content');
                                    $tabContent.each(function () {
                                        var $el = $(this);
                                        $el.removeClass('-active');
                                        if($el.data('tab') == numberTab)
                                            $el.addClass('-active');
                                    });
                                }
                            ]
                        ];
                    },
                    // Вызывается при загрузке странице
                    initialize: function ($target) {
                    }
                }
            )
        );
    }()
);