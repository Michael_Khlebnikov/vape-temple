(
    function () {
        // Заводим новый блок
        var catalogBlock = {};

        // Регистрация
        WindCore.blockRegister(
            // Новый блок объединяется с существующим "WindBlock"
            $.extend(
                catalogBlock,
                WindBlock, {
                    // Устанавливаем родительский блок
                    getSelector: function () {
                        return '.js-catalog';
                    },
                    // Возвращает массив объектов с которыми работаем
                    getBindings: function () {
                        // Сохраняем текущий контекст
                        var self = this;
                        return [
                            // Клик на кнопку открывающую список с параметрами сортировки
                            [
                                'click',
                                '.js-sorting-controller',
                                function (event) {
                                    event.preventDefault();
                                    $('.js-sorting-list').toggleClass('-hide');
                                }
                            ]
                        ];
                    },
                    // Вызывается при загрузке странице
                    initialize: function ($target) {
                    }
                }
            )
        );
    }()
);