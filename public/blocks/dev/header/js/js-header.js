(
    function () {
        // Заводим новый блок
        var headerBlock = {};

        // Регистрация
        WindCore.blockRegister(
            // Новый блок объединяется с существующим "WindBlock"
            $.extend(
                headerBlock,
                WindBlock, {
                    // Устанавливаем родительский блок
                    getSelector: function () {
                        return '.js-header';
                    },
                    // Возвращает массив объектов с которыми работаем
                    getBindings: function () {
                        // Сохраняем текущий контекст
                        var self = this;
                        return [
                            // Скрываем аутентификацию при клике на оверлэй
                            [
                                'click',
                                '.js-auth-controller',
                                function (event) {
                                    event.preventDefault();
                                    $('.b-auth').removeClass('-hide');
                                }
                            ]
                        ];
                    },
                    // Вызывается при загрузке странице
                    initialize: function ($target) {
                    }
                }
            )
        );
    }()
);